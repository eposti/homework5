
import java.util.*;

public class Node {

   private String name;
   private Node firstChild;
   private Node nextSibling;

   Node (String n, Node d, Node r) {
      // TODO!!! Your constructor here
      this.name = n;
      this.firstChild = d;
      this.nextSibling = r;
   }
   
   public static Node parsePostfix (String s) {
      Node parsedNode = innerParse(s);

      if(parsedNode.nextSibling != null) {
         throw new RuntimeException("Juurel ei tohi olla vendasid");
      }

      return parsedNode;
   }

   private static Node innerParse(String s) {
      List<String> nodeStrings = findSubNodeStrings(s);

      if(nodeStrings.size() <= 0 || s.trim().length() <= 0) {
         throw new RuntimeException("Node ei tohi olla tühi string");
      }

      Node lastCreatedNode = null;

      for (String nodeString : nodeStrings) {
         lastCreatedNode = subNodeStringToNode(nodeString, lastCreatedNode);
      }

      return lastCreatedNode;
   }

   public static List<String> findSubNodeStrings(String s) {

      int currentNodeStartIndex = 0;
      int currentLevel = 0;

      List<String> nodeStrings = new ArrayList<>();

      for (int i = 0; i < s.length(); i++){
         char c = s.charAt(i);

         if(c == '(') {
            currentLevel ++;
         } else if(c == ')') {
            currentLevel --;
         } else if(c == ',') {
            if(currentLevel == 0) {
               nodeStrings.add(s.substring(currentNodeStartIndex, i));
               currentNodeStartIndex = i + 1;
            }
         }
      }

      nodeStrings.add(s.substring(currentNodeStartIndex, s.length()));

      Collections.reverse(nodeStrings);

      return nodeStrings;
   }

   private static Node subNodeStringToNode(String s, Node sibling) {
      int currentLevel = 0;

      int childrenStartIndex = 0;
      int childrenEndIndex = 0;

      Boolean hasChildren = false;

      String name = "";

      for (int i = 0; i < s.length(); i++){
         char c = s.charAt(i);

         if(c == '(') {
            if(currentLevel == 0) {
               childrenStartIndex = i + 1;
            }

            currentLevel ++;
            hasChildren = true;
         } else if(c == ')') {
            currentLevel --;

            if(currentLevel == 0) {
               childrenEndIndex = i;
            }
         } else if(c != ',') {
            if(currentLevel == 0) {
               name += c;
            }
         }
      }

      Node child = null;

      if(hasChildren) {
         child = innerParse(s.substring(childrenStartIndex, childrenEndIndex));
      }

      if(name.length() <= 0) {
         throw new RuntimeException("Node-i puudub väärtus: " + s);
      }

      if(name.contains(" ")) {
         throw new RuntimeException("Node-i väärtus ei tohi sisaldada tühikuid: " + s);
      }

      return new Node(name, child, sibling);
   }

   public String leftParentheticRepresentation() {

      String builtString = "";

      builtString += this.name;

      if(this.firstChild != null) {
         builtString += "(";

         builtString += this.firstChild.leftParentheticRepresentation();

         builtString += ")";
      }

      if(this.nextSibling != null) {

         builtString += ",";

         builtString += this.nextSibling.leftParentheticRepresentation();
      }

      return builtString;
   }

   public static void main (String[] param) {
      String s = "(B1,C)A";

      Node t = Node.parsePostfix (s);
      String v = t.leftParentheticRepresentation();
      System.out.println (s + " ==> " + v); // (B1,C)A ==> A(B1,C)
   }
}

